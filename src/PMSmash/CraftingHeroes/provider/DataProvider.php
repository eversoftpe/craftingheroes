<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes\provider;

use PMSmash\CraftingHeroes\Arena\Arena;
use PMSmash\CraftingHeroes\CraftingHeroes;
use PMSmash\CraftingHeroes\Utils\PluginUtils;
use pocketmine\Player;
use pocketmine\Server;
use SQLite3;

class DataProvider extends SQLite3
{
    private $db;
    private $main;
    public $names = array();

    /**
     * DataProvider constructor.
     * @param CraftingHeroes $main
     */
    public function __construct(CraftingHeroes $main)
    {
        $this->main = $main;
        $this->db = new SQLite3($this->main->getFolder() . DIRECTORY_SEPARATOR . 'database.sq3');
        $this->db->exec('CREATE TABLE IF NOT EXISTS Arenas (
            ID INT PRIMARY KEY,
            NAME TEXT NOT NULL,
            SLOTS TEXT NOT NULL,
            SPAWN VARCHAR NOT NULL,
            LIVING TEXT NOT NULL
        )');
        $sql = $this->db->query('SELECT NAME FROM Arenas');
        while ($row = $sql->fetchArray(SQLITE3_ASSOC)) {
            foreach ($row as $item) {
                $this->names[] = $item;
            }
        }
        foreach($this->names as $loadworld) {
            Server::getInstance()->loadLevel($loadworld);
        }
    }

    /**
     * @return SQLite3
     */
    public function getDatabase() : SQLite3
    {
        return $this->db;
    }

    /**
     * @param Player $player
     * @param int $slots
     * @param string $spawn
     */
    public function setArena(Player $player, int $slots = 4, string $spawn)
    {
        $arena = new Arena($this->main);
        $sql = $this->db->prepare('INSERT INTO Arenas (NAME, SLOTS, SPAWN) SELECT :arenaName, :slots, :spawn WHERE NOT EXISTS (SELECT * FROM Arenas WHERE NAME = :arenaName AND SLOTS = :slots AND SPAWN = :spawn)');
        $sql->bindValue(':arenaName', $arena->getPlayerArenaName($player));
        $sql->bindValue(':slots', $slots);
        $sql->bindValue(':spawn', $arena->getArenaSpawn($spawn));
        $sql->execute();
    }

    /**
     * @param Player $player
     */
    public function getArenas(Player $player)
    {
        $sql = $this->getDatabase()->query('SELECT NAME FROM Arenas');
        $player->sendMessage(PluginUtils::ARENAS_LIST);
        while ($row = $sql->fetchArray(SQLITE3_ASSOC)) {
            $player->sendMessage('> ' . $row['NAME']);
        }
    }

    /**
     * @param Player $player
     * @param string $arenaName
     */
    public function deleteArena(Player $player, string $arenaName)
    {
        $sql = $this->db->prepare('DELETE FROM Arenas WHERE NAME = :arenaName');
        $sql->bindValue(':arenaName', $arenaName);
        $sql->execute();
        $player->sendMessage(PluginUtils::ARENA_DELETED . $arenaName);
    }

    /**
     * @return array
     */
    public function getArenaNames(): array
    {
        $sql = $this->db->query('SELECT NAME FROM Arenas');
        while ($row = $sql->fetchArray(SQLITE3_ASSOC)) {
            foreach ($row['NAME'] as $item) {
                $this->names = $item;
            }
        }
        return $this->names;
    }

    /**
     * @param string $arena
     * @return int
     */
    public function getPlayersInGame(string $arena): int {
        $number_of_players = count($this->main->getServer()->getLevelByName($arena)->getPlayers());
        return $number_of_players;
    }

    /**
     * @param string $arenaName
     */
    public function setLiving(string $arenaName){
        $sql = $this->db->prepare('INSERT INTO Arenas (LIVING) VALUES (:worldName)');
        $sql->bindValue(':worldName', $arenaName);
        $sql->execute();
    }

    /**
     * @return mixed
     */
    public function getLiving(): string{
        $living = [];
        $sql = $this->getDatabase()->prepare('SELECT LIVING FROM Arenas');
        $result = $sql->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $living[] = $row['LIVING'];
        }
        $level = $living[0];
        $tp = Server::getInstance()->getLevelByName($level)->getSpawnLocation();
        return $tp;
    }


    /**
     * @return int
     */
    public function getNumberOfArenas(): int{
        $sql = $this->getDatabase()->query('SELECT COUNT(NAME) as num FROM Arenas');
        $result = $sql->fetchArray(SQLITE3_ASSOC);
        $num = $result['num'];
        return $num;
}

}