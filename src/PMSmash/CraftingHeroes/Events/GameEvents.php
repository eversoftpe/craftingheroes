<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes\Events;

use PMSmash\CraftingHeroes\CraftingHeroes;
use PMSmash\CraftingHeroes\Forms\DataSender;
use PMSmash\CraftingHeroes\provider\DataProvider;
use pocketmine\entity\Human;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\Listener;
use pocketmine\Player;
use pocketmine\utils\TextFormat as TE;

class GameEvents implements Listener
{
    private $main;
    private $data_sender;

    /**
     * GameEvents constructor.
     * @param CraftingHeroes $main
     */
    public function __construct(CraftingHeroes $main)
    {
        $this->main = $main;
        $this->data_sender = new DataSender($main);
    }
    /**
     * @param EntityDamageByEntityEvent $event
     */
    public function onHitNPC(EntityDamageByEntityEvent $event)
    {
        $player = $event->getDamager();
        $npc = $event->getEntity();
        if ($player instanceof Player and $npc instanceof Human) {
            if ($npc->getNameTag() == TE::BOLD . TE::DARK_PURPLE . "Crafting" . TE::GRAY . "Heroes" . "\n§6Toca para jugar!") {
                $event->setCancelled(true);
                $this->data_sender->addMenu($player);
            }
        }
    }

}