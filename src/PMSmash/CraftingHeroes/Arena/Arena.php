<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes\Arena;


use PMSmash\CraftingHeroes\CraftingHeroes;
use PMSmash\CraftingHeroes\provider\DataProvider;
use pocketmine\level\Position;
use pocketmine\Player;
use pocketmine\Server;

class Arena
{
    /**
     * Arena Variables
     * @var $main
     * @var $slots;
     */

    private $main;
    public $slots = 4;
    public $players = array();
    private $data_provider;

    /**
     * Arena constructor.
     * @param CraftingHeroes $main
     */
    public function __construct(CraftingHeroes $main)
    {
        $this->main = $main;
        $this->data_provider = new DataProvider($main);
    }

    /**
     * @param Player $player
     * @return string
     */
    public function getPlayerArenaName(Player $player) : string {
        return $player->getLevel()->getName();
    }


    /**
     * @param string $arenaName
     * @return Position
     */
    public function getArenaSpawn(string $arenaName): Position {
        return Server::getInstance()->getLevelByName($arenaName)->getSpawnLocation();
    }

    /**
     * @return int
     */
    public function getSlots() : int {
        return $this->slots;
    }

    public function teleportToArena(Player $player, string $arenaName){
        $arena = [];
        $sql = $this->data_provider->getDatabase()->prepare('SELECT * FROM Arenas WHERE NAME = :arenaName');
        $sql->bindValue(':arenaName', $arenaName);
        $result = $sql->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $arena[] = $row['NAME'];
        }
        $level = $this->getArenaSpawn($arena[0]);
        $player->teleport($level);
    }

    public function teleportToLiving(Player $player){
        $living = $this->data_provider->getLiving();
        $player->teleport($living);
    }

}