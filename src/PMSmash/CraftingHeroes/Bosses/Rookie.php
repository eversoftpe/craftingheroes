<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/


namespace PMSmash\CraftingHeroes\Bosses;

use pocketmine\entity\Skin;
use pocketmine\Player;
use PMSmash\CraftingHeroes\CraftingHeroes;
use pocketmine\utils\TextFormat as TE;

class Rookie
{
    public $main;

    /**
     * Rookie constructor.
     * @param CraftingHeroes $main
     */
    public function __construct(CraftingHeroes $main)
    {
        $this->main = $main;
    }

    /**
     * @param Player $player
     * @param string $file
     * @param string $ex
     * @param string $geo
     */
    public function SetSkin(Player $player, string $file, string $ex, string $geo) {
        $skin = $player->getSkin();
        $path = $this->main->getFolder() . $file . $ex;
        $img = @imagecreatefrompng($path);
        $skinbytes = "";
        $s = (int)@getimagesize($path)[1];
        for($y = 0; $y < $s; $y++){
            for($x = 0; $x < 128; $x++){
                $colorat = @imagecolorat($img, $x, $y);
                $a = ((~((int)($colorat >> 24))) << 1) & 0xff;
                $r = ($colorat >> 16) & 0xff;
                $g = ($colorat >> 8) & 0xff;
                $b = $colorat & 0xff;
                $skinbytes .= chr($r) . chr($g) . chr($b) . chr($a);
            }
        }
        @imagedestroy($img);
        $player->setSkin(new Skin($skin->getSkinId(), $skinbytes, "", "geometry.pmsmash." . $geo, file_get_contents($this->main->getFolder() . "rookie.json")));
        $player->sendSkin();
        $player->sendMessage(TE::GREEN . "Ahora eres " . $file);
    }
}
