<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes;

use PMSmash\CraftingHeroes\Commands\Commands;
use PMSmash\CraftingHeroes\Events\GameEvents;
use PMSmash\CraftingHeroes\provider\DataProvider;
use PMSmash\CraftingHeroes\Utils\PluginUtils;
use pocketmine\plugin\PluginBase;

class CraftingHeroes extends PluginBase
{

    public $main;
    public $data_provider;

    /**
     * @onEnable() function
     */
    public function onEnable(): void
    {
        foreach (["Rookie.png", "rookie.json", "Slendy.png", "slendy.json", "database.sq3"] as $file) {
            $this->saveResource($file);
        }
        $this->data_provider = new DataProvider($this);
        PluginUtils::onLog(PluginUtils::CONNECTED);
        $this->getServer()->getCommandMap()->register("ch", new Commands($this));
        PluginUtils::onLog(PluginUtils::ENABLED);
        $this->getServer()->getPluginManager()->registerEvents(new GameEvents($this), $this);
    }

    /**
     * @onLoad() function
     */
    public function onLoad()
    {
        PluginUtils::onWarn(PluginUtils::LOADING_ARENAS);
        PluginUtils::onWarn(PluginUtils::LOADING_DB);
    }

    /**
     * @onDisable function
     */
    public function onDisable()
    {
        PluginUtils::onError(PluginUtils::DISABLED);
    }

    /**
     * @return string
     */
    public function getFolder(): string
    {
        return $this->getDataFolder();
    }

}
