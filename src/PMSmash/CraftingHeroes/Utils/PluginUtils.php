<?php

/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes\Utils;
use pocketmine\Server;
use pocketmine\utils\TextFormat as TE;

class PluginUtils
{

    const PREFIX = TE::DARK_RED . '[' . TE::BOLD . TE::DARK_PURPLE . 'Crafting' . TE::GRAY . 'Heroes' . TE::RESET . TE::DARK_RED . ']' . TE::RESET;
    const CONNECTED = TE::GREEN . 'Base de datos conectada con exito!';
    const ENABLED = TE::GREEN . 'CraftingHeroes Activado!';
    const DISABLED = TE::RED . 'CraftingHeroes Desactivado!';
    const EMPTY = self::PREFIX . TE::RED . 'No hay arenas!';
    const LOADING_ARENAS = TE::YELLOW . 'Cargando Arenas...';
    const LOADING_DB = TE::YELLOW . 'Creando conexion SQLite...';
    const ARENA_SET_USAGE = self::PREFIX . TE::RED . 'Usage: /ch arena set <str: arenaName>';
    const ARENA_USAGE = self::PREFIX . TE::RED . 'Usage: /ch arena set|delete|list';
    const ARENA_DELETE_USAGE = self::PREFIX . TE::RED . 'Usage: /ch arena delete <str: arenaName>';
    const DELETE_USAGE = self::PREFIX . TE::RED . 'Usage: /ch arena delete <str: arenaName>';
    const USE_IN_GAME = self::PREFIX . TE::RED . 'Usa el comando en juego!';
    const ARENAS_LIST = TE::GOLD . '>>-----' . self::PREFIX . TE::GOLD . '-----<<';
    const FORM_TITLE = TE::BOLD . TE::DARK_PURPLE . 'Crafting' . TE::GRAY . 'Heroes';
    const SELECT_GAME = TE::GREEN . 'Selecciona un juego!';
    const BE_IN_ARENA = self::PREFIX . TE::RED . 'Usa el comando dentro del mundo!';
    const ARENA_CREATED = self::PREFIX . TE::GREEN . 'Haz creado la arena: ';
    const ARENA_DELETED = self::PREFIX . TE::GREEN . 'Haz borrado la arena: ';
    const NPC_PLACED = self::PREFIX . TE::GREEN . 'Haz puesto el NPC del juego!';

    /**
     * @param string $message
     * @return mixed
     */
    public static function onLog(string $message) {
        $logger = Server::getInstance()->getLogger();
        return $logger->info(self::PREFIX . TE::GREEN . $message);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public static function onWarn(string $message) {
        $logger = Server::getInstance()->getLogger();
        return $logger->info(self::PREFIX . TE::YELLOW . $message);
    }

    /**
     * @param string $message
     * @return mixed
     */
    public static function onError(string $message) {
        $logger = Server::getInstance()->getLogger();
        return $logger->info(self::PREFIX . TE::RED . $message);
    }
}