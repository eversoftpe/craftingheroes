<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/

namespace PMSmash\CraftingHeroes\Commands;

use PMSmash\CraftingHeroes\Arena\JoinNPC;
use PMSmash\CraftingHeroes\CraftingHeroes;
use PMSmash\CraftingHeroes\provider\DataProvider;
use PMSmash\CraftingHeroes\Utils\PluginUtils;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\PluginIdentifiableCommand;
use pocketmine\Player;
use pocketmine\plugin\Plugin;

class Commands extends Command implements PluginIdentifiableCommand
{
    public $plugin;
    public $main;

    /**
     * Commands constructor.
     * @param CraftingHeroes $main
     */
    public function __construct(CraftingHeroes $main)
    {
        parent::__construct('ch', '§aCrafting Heroes commands', '/ch help', ['ch', 'heroes']);
        $this->main = $main;
    }

    /**
     * @return Plugin
     */
    public function getPlugin(): Plugin
    {
        return $this->plugin;
    }

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param array $args
     * @return mixed|void
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args)
    {
        $data_provider = new DataProvider($this->main);
        if ($sender instanceof Player) {
            switch ($args[0]) {
                case 'living':
                    $living = $sender->getLevel()->getName();
                    $data_provider->setLiving($living);
                    break;

                case 'arena':
                    if (!isset($args[1])) {
                        $sender->sendMessage(PluginUtils::ARENA_USAGE);
                    } elseif ($args[1] == 'set') {
                        if (!isset($args[2])) {
                            $sender->sendMessage(PluginUtils::ARENA_SET_USAGE);
                        } elseif ($sender->getLevel()->getName() != $args[2]) {
                                $sender->sendMessage(PluginUtils::BE_IN_ARENA);
                        } else {
                            $data_provider->setArena($sender->getPlayer(), 4, $args[2]);
                            $sender->sendMessage(PluginUtils::ARENA_CREATED . $args[2]);
                        }
                    } elseif ($args[1] == 'list'){
                        $data_provider->getArenas($sender->getPlayer());
                    } elseif ($args[1] == 'delete'){
                        if (!isset($args[2])) {
                            $sender->sendMessage(PluginUtils::DELETE_USAGE);
                        } else {
                            $data_provider->deleteArena($sender->getPlayer(), $args[2]);
                        }
                    } else {
                        $sender->sendMessage(PluginUtils::ARENA_USAGE);
                    }
                    break;

                case 'npc':
                    $start = new JoinNPC($this->main);
                    $start->spawnNPC($sender->getPlayer());
                    $sender->sendMessage(PluginUtils::NPC_PLACED);
                    break;

            }
        }
        else {
            $sender->sendMessage(PluginUtils::USE_IN_GAME);
        }
    }
}
