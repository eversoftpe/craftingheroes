<?php
/*
 *    #####                                             #     #
 *    #     # #####    ##   ###### ##### # #    #  ####  #     # ###### #####   ####  ######  ####
 *    #       #    #  #  #  #        #   # ##   # #    # #     # #      #    # #    # #      #
 *    #       #    # #    # #####    #   # # #  # #      ####### #####  #    # #    # #####   ####
 *    #       #####  ###### #        #   # #  # # #  ### #     # #      #####  #    # #           #
 *    #     # #   #  #    # #        #   # #   ## #    # #     # #      #   #  #    # #      #    #
 *    #####  #    # #    # #        #   # #    #  ####  #     # ###### #    #  ####  ######  ####
 *
 * This file is part of CraftingHeroes.
 *
 * CraftingHeroes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Apache General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CraftingHeroes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Apache General Public License
 * along with CraftingHeroes.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author EversoftPE
 * @link https://gitlab.com/eversoftpe
 *
*/
namespace PMSmash\CraftingHeroes\Forms;

use PMSmash\CraftingHeroes\CraftingHeroes;
use PMSmash\CraftingHeroes\provider\DataProvider;
use PMSmash\CraftingHeroes\Utils\PluginUtils;
use pocketmine\Player;

class DataSender {

    private $main;
    private $data_provider;

    public function __construct(CraftingHeroes $main)
    {
        $this->main = $main;
        $this->data_provider = new DataProvider($main);
    }

    public function sendData(){
        $data = array(
            "type"    => "form",
            "title"   => PluginUtils::FORM_TITLE,
            "content" => PluginUtils::SELECT_GAME,
            "buttons" => array()
        );
        foreach($this->data_provider->names as $name){
            $data["buttons"][] = array("text" => $name);
        }
        return $data;
    }

    public function addMenu(Player $player){
        $accion = function($player,$data){
            $name = $this->data_provider->names;
            if($data == null){
                return;
            }
            if ($player instanceof Player) {
                switch ($name[$data]) {
                    case $name[0]:
                        $player->sendMessage($name[0]);
                        break;
                    case $name[1]:
                        $player->sendMessage($name[1]);
                        break;
                    case $name[2]:
                        $player->sendMessage($name[2]);
                        break;
                    case $name[3]:
                        $player->sendMessage($name[3]);
                        break;
                    case $name[4]:
                        $player->sendMessage($name[4]);
                        break;
                    case $name[5]:
                        $player->sendMessage($name[5]);
                        break;
                    case $name[6]:
                        $player->sendMessage($name[6]);
                        break;
                    case $name[7]:
                        $player->sendMessage($name[7]);
                        break;
                    case $name[8]:
                        $player->sendMessage($name[8]);
                        break;
                    case $name[9]:
                        $player->sendMessage($name[9]);
                        break;
                    case $name[10]:
                        $player->sendMessage($name[10]);
                        break;
                }
            }
        };
        $player->sendForm(new DataForm($this->sendData(),$accion));

    }
}