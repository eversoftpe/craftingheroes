**Crafting Heroes    **                           

Prerequisitos:
 - Extension GD habilitada en php.ini (Por defecto habilitada en los binarios php de Jenkins PMMP)
 - Extension pdo_sqlite habilitada en php.ini (Por defecto habilitada en los binarios php de Jenkins PMMP)
 
Instalacion:
 - Colocar el plugin en la carpeta /plugins del servidor
 - Configurar arenas dentro del juego y ubicar el NPC de juego.
 
Versionado: 
 - Compatible con API 3.0.0 para MCPE 1.8+ y Pocketmine 3.0.0+
 
Autores:
 - El software esta desarrollado por EverSoft Team.
  - Contribuyentes: Twitter @PocketmineSmash / @SoyPlayek
 
Licencia:
 - Este proyecto se encuentra bajo la licencia Apache 2.0.0, vea mas informacion en LICENSE.md

**Para hacer **

- [x] soporte Multi-Arena
- [ ] Añadir bosses
    - [x] Rookie
    - [x] Slendy
    - [ ] Troll
    - [ ] Warmber
    - [ ] Insept
- [ ] Form Games
- [ ] Change dimension in games